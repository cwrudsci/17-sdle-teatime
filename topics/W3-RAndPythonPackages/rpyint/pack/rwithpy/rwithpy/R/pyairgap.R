#' pyairgap
#'
#' Function description.
#'
#' @name pyairgap
#' @param x The first parameter.
#' @param y The second parameter.
#' @return What is the returned value, if any.
#' @export
#' @examples
#' pyairgap(2,1)
#' pyairgap(1,0)
pyairgap <- function(a1, a2, a3){
  path2script <- system.file("pyscripts", "pyag.py", package = "rwithpy")
  allArgs <- paste(c("python", path2script, paste("'",a1,"'",sep=""), paste("'",a2,"'",sep="")),collapse=" ")
  system(allArgs)
  faddr <- "py-to-r.csv"
  res <- read.csv(faddr,header=FALSE); file.remove(faddr)
  return(res)
}

