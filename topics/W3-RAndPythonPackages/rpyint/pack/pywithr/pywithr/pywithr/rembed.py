def rembed(a,b):
    """This function adds two numbers together.

    Args:
        arg1 (numeric): The first parameter.
        arg2 (numeric): The second parameter.

    Returns:
        res (numeric): The result of adding the two input parameters together.
    """
    import rpy2.robjects as robjects
    robjects.globalenv["a"] = a
    robjects.globalenv["b"] = b
    res = robjects.r('''a+b''')[0]
    return(res)
