def plotsin():
    """This function adds two numbers together.

    Args:
        arg1 (numeric): The first parameter.
        arg2 (numeric): The second parameter.

    Returns:
        res (numeric): The result of adding the two input parameters together.
    """
    import numpy as np
    import matplotlib.pyplot as plt
    x = np.linspace(0,10,1e6);
    y = np.sin(x)
    plt.plot(x,y)
