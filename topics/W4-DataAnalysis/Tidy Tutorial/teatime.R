setwd("vignettes/")
preg <- read.csv("preg.csv", stringsAsFactors = FALSE)
preg
preg2 <- preg %>%
  gather(treatment,value, treatmenta:treatmentb)
preg2

preg2 <- preg %>%
  gather(treatment, value, treatmenta:treatmentb) %>%
  mutate(treatment_mod=gsub("treatment","",treatment))
preg2


preg2 <- preg %>%
  gather(treatment, value, treatmenta:treatmentb) %>%
  mutate(treatment = gsub("treatment", "", treatment))
preg2

preg2 <- preg %>%
  gather(treatment, value, treatmenta:treatmentb) %>%
  mutate(treatment = gsub("treatment", "", treatment)) %>%
  arrange(name,treatment)
  preg2

  pew <- tbl_df(read.csv("pew.csv", stringsAsFactors = FALSE, check.names = FALSE))
  pew

  pew2 <- pew %>%
    gather(income, frequency, -religion)
pew2


billboard <- tbl_df(read.csv("billboard.csv", stringsAsFactors = FALSE))
billboard


billboard2 <- billboard %>%
  gather(week, rank, wk1:wk76, na.rm = TRUE)
billboard2

billboard3 <- billboard2 %>%
  mutate(
    week = readr::parse_number(week),
    date = as.Date(date.entered) + 7 * (week - 1)) %>%
  select(-date.entered)
billboard3


billboard3 %>% arrange(artist, track, week)
billboard3

tb <- tbl_df(read.csv("tb.csv", stringsAsFactors = FALSE))
tb

tb2 <- tb %>%
  gather(demo, n, -iso2, -year, na.rm = TRUE)
tb2

tb3 <- tb2 %>%
  separate(demo, c("sex", "age"), 1)
tb3


weather <- tbl_df(read.csv("weather.csv", stringsAsFactors = FALSE))
weather


weather2 <- weather %>%
  gather(day, value, d1:d31, na.rm = TRUE)
weather2

weather3 <- weather2 %>%
  mutate(day = readr::parse_number(day)) %>%
  select(id, year, month, day, element, value) %>%
  arrange(id, year, month, day)
weather3

weather3 %>% spread(element, value)
weather3


song <- billboard3 %>%
  select(artist, track, year, time) %>%
  unique() %>%
  mutate(song_id = row_number())
song


rank <- billboard3 %>%
  left_join(song, c("artist", "track", "year", "time")) %>%
  select(song_id, date, week, rank) %>%
  arrange(song_id, date)
rank


library(plyr)
paths <- dir("data", pattern = "\\.csv$", full.names = TRUE)
names(paths) <- basename(paths)
ldply(paths, read.csv, stringsAsFactors = FALSE)
