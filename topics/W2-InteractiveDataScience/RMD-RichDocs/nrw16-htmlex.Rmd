---
title: "PDF RMD"
author: Nick Wheeler
date: June 22, 2017
output: html_document
---

# First level header

```{r}
print(1+2)
```

here is one line of text <br><br>here is another line of text


<h1>My First JavaScript</h1>

<button type="button"
onclick="document.getElementById('demo').innerHTML = Date()">
Click me to display Date and Time.</button>

<p id="demo"></p>

```{r results='asis'}
c1 <- toupper(letters[1:5])
c2 <- round(rnorm(5),2)
mydat <- data.frame("A"=c1,"B"=c2)
htmlTable::htmlTable(mydat)
```

```{r}
library("threejs")
earth <- "http://eoimages.gsfc.nasa.gov/images/imagerecords/73000/73909/world.topo.bathy.200412.3x5400x2700.jpg"
globejs(img=earth, bg="white")
```

