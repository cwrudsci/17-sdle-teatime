# This is the README for the 2017 CWRU SDLE Tea-time Learnings Git Repository

## SDLE Tea-times: 3:30 to 4:15, on Tuesdays, Wednesdays and Thursdays
## Starting Tuesday June 6th, through Thursday July 20th, 2017
## A set of teachings of R, Python, Git, LaTeX for Data Science.
### Any interested people are invited. 
### These are intended to be simple startups in these topics.
### These are relatively informal, but structured. 

## Here we will share  datasets, R codes and markdown reports. 
### SDLE Tea-time Videos are on [Periscope (Live)] [9] and [YouTube] [10]

## Project Title: SDLE Tea-time Learnings: R, Python, Git & LaTeX
 
## Authors: R. H. French, in collaboration with the students of the Case Western Reserve University SDLE Research Center 

[Case Western Reserve University, SDLElab] [1]
 
### [SDLE REDCap Sample Database Login] [3]
### [The R Project for Statistical Computing] [4]
### [RStudio Integragrated Development Enviroment (IDE) for R] [5]
### [RMarkdown for open science collaboration & reporting] [6]
### [Frenchrh @frenchrh on Twitter] [7]
### [SDLE_ResCntr @SDLE_ResCntr on Twitter] [8]


[1]: http://sdle.case.edu
[2]: 
[3]: https://dcru.case.edu/redcap/
[4]: http://www.r-project.org/
[5]: http://www.rstudio.com/products/RStudio/
[6]: http://rmarkdown.rstudio.com/
[7]: https://twitter.com/frenchrh
[8]: https://twitter.com/SDLE_ResCntr
[9]: https://www.periscope.tv/SDLE_ResCntr/1RDxlwgLavgJL
[10]: https://www.youtube.com/playlist?list=PLBrrkqzxVsYjiVi9W-3j_bcxgWNq7RqXG
