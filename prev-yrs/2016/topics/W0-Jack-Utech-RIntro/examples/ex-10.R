
dates <- seq(as.POSIXct("2010-10-18"), as.POSIXct("2016-10-18"), by = "1 hour")
dates <- as.character(dates)

ncores <- parallel::detectCores()
cluster <- parallel::makeCluster(ncores - 1)

system.time(parallel::parSapply(cluster, dates, function(date) {
    return(as.POSIXct(date))
}))

parallel::stopCluster(cluster)
