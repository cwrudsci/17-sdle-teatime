# Script Name: tea_time_class_5.R
# Purpose: Packge Creation
# Authors: Ethan Pickering 
# License: Creative Commons Attribution-NonCommercial-ShareAlike- 4.0 International License.  
# Latest Changelog Entires: v0.00.01 - tea_time_class_5.R - Ethan Started it



# We Need package devtools

install.packages("devtools")
library(devtools)

# 1) File New Project - Exisitng Directory - R Package

# Name package for your purpose, and create - there are other options
# you could choose such as creating a repo for the package.

# You will see a new folder with a bunch of "package stuff" - lets browse through it


# Lets make our own function

teatime = function(x, person){
  
  # Use the print function
  print(paste(person, ", I thought there would be Tea at 'Tea Time'?", sep = ""))
  
  
  # Lets solve an equation as well so we can return it
  y = x^2
  
  
  return(y)
}


#Need Roxygen2 package for automated package development

# Make a help file using "roxygen2" comments


#'Tea Time
#'
#'\code{teatime} Does Tea Time stuff
#'
#'This function reads in an x value or array and returns the values squared. It also talks to you.
#'
#' @param x value or array that is inputted
#' @param person name of the person you want to embarass
#'
#' @return the values of x^2
#'
#' @examples
#' input a set of x values and the name of a person
#' y = teatime(2, "Roger")
#' @export teatime
#' 
#' 

devtools::document()


# So we have a function and we have a help file, lets build the package

devtools::build()


# Now we have succesfully built a package, lets install it
# We have to use some special commands since it is not a CRAN package
install.packages("H:/git/16-sdle-teatime/code/packages/sdleteatime_0.1.0.tar.gz", repos = NULL, type = "source", lib = ".")

library("sdleteatime", lib = ".")


# Example of the EDIFES package

.libPaths("H:/Downloads/teatime")

devtools::install_bitbucket("cwrusdle/16-edifes", auth_user = "Ethan_Pickering", password = "", subdir = "edifes")

library(edifes)
